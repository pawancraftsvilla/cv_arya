import pandas as pd
from operator import itemgetter
from RedshiftModule.config import config
from RedshiftModule.redshift_queries import queries
# import time
import psycopg2


class Arya(config):

    def __init__(self, config):
        self.redshift = config.REDSHIFT_CONNECTION

    def read_sql_query(self, query):
        return pd.read_sql_query(query, self.redshift)

    def get_data_from_redshift(self, category_id, conversion_weight, gmv_weight, dispute_weight):
        '''Gets data from redshift'''
        query = None

        # if attribute_name and attribute_value:
        #     query = 'get_product_with_attribute'

        # elif attribute_name and not attribute_value:
        #     query = 'get_product_without_attribute_value'

        # elif not attribute_name and not attribute_value:
        #     query = 'get_product_without_attribute'

        query = 'get_top_products'
        
        q = queries[query].format(conversion_weight, gmv_weight, dispute_weight, category_id)
        print(q)

        return self.read_sql_query(q)


    def get_data(self, category_id=None, conversion_weight=None, gmv_weight=None, dispute_weight=None):

        if category_id==None:
            return None
        product_data = self.get_data_from_redshift(category_id, conversion_weight,
                     gmv_weight, dispute_weight)

        print(type(product_data))

        colorder = [
            'product_id',
            'relevance_score'
        ]

        return product_data
        # return product_data[colorder].T.to_json(orient='values')

    def get_data_for_product_health(self, start_date, end_date):

        q = queries['mormont_product_health_data'].format(start_date, end_date)
        count_data =  self.read_sql_query(q)
        count_data['date'] = count_data['date'].apply(lambda x: x.strftime('%Y-%m-%d'))
        return count_data.reset_index().T.to_json(orient='values')