queries = {
	"get_top_products" : """ 
			SELECT 
				product_id,
				(conversion * {0} + gmv * {1} - dispute_percentage * {2}) AS relevance_score 
			FROM cv_relevance_score 
			WHERE category_id={3}
				AND conversion IS NOT NULL 
				AND gmv IS NOT NULL
			ORDER BY relevance_score DESC
			LIMIT 100;
	"""
}