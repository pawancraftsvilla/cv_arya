import psycopg2

def get_redshift_connection(conf):
    REDSHIFT_CONF = conf
    REDSHIFT_SERVER = REDSHIFT_CONF.get('host')
    REDSHIFT_PORT = REDSHIFT_CONF.get('port')
    REDSHIFT_USER = REDSHIFT_CONF.get('user')
    REDSHIFT_PASSWORD = REDSHIFT_CONF.get('password')
    REDSHIFT_DB = REDSHIFT_CONF.get('database')
    if REDSHIFT_SERVER and REDSHIFT_PORT and REDSHIFT_USER and REDSHIFT_DB:
        REDSHIFT_connection = psycopg2.connect(
            host=REDSHIFT_SERVER, user=REDSHIFT_USER, port=REDSHIFT_PORT, password=REDSHIFT_PASSWORD, database=REDSHIFT_DB)
        return REDSHIFT_connection
    else:
        raise ErrorMessage("REDSHIFT credential not sufficient")