import json
from flask import Flask, request, render_template
from RedshiftModule.arya import Arya
from RedshiftModule.config import config

app = Flask(__name__)
a = Arya(config)

@app.route('/')
def homepage():
    return render_template('index.html')


@app.route('/retrieve_products', methods=['POST'])
def products():
	price_score = request.form['price_score']
	conversion = request.form['conversion']
	gmv = request.form['gmv']
	dispute = request.form['dispute']
	rto = request.form['rto']
	category_id = request.form['category']
	print(category_id)

	d = a.get_data(category_id, conversion, gmv, dispute)
	# d = json.loads(a.get_data(category_id, conversion, gmv, dispute))
	print(d)

	return render_template('index.html', result =d.to_html())